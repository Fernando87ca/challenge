package com.db.awmd.challenge.exception;

public class InsufficientAmountException extends RuntimeException {

    /**
     * Retrieve an exception when the user don't have enough money to perform the transaction
     */
    public InsufficientAmountException(String message) {
        super(message);
    }
}
