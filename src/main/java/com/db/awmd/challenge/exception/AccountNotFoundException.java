package com.db.awmd.challenge.exception;

public class AccountNotFoundException extends RuntimeException {

    /**
     * Retrieve an exception when is not possible find the user in
     */
    public AccountNotFoundException(String message) {
        super(message);
    }
}
