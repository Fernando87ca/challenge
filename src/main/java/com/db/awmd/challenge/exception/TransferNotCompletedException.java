package com.db.awmd.challenge.exception;

public class TransferNotCompletedException extends RuntimeException {

    /**
     * Retrieve an exception when is not possible complete the transaction with unexpected reason, real example:
     * When database is down on the middle of an operation and a new transaction on same origin id account is waiting till lock timeout.
     */
    public TransferNotCompletedException(String message) {
        super(message);
    }
}
